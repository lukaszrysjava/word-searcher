# Word Searcher
Application for searching words in files in a given directory.

## Requirements
* Java 11

## Running
* `./mvnw clean install`
* `java -jar target/word-searcher.jar ${DIRECTORY_PATH}`
* To terminate write `:quit`

## Assumptions

* The application takes the path to the directory as an input argument.
* Text files have the extension ".txt".
* A word is a string containing only letters.
* A word with a digit is considered to be an incorrect word - therefore it is ignored.
* A word with punctuation marks is considered valid, but these characters are ignored. For instance, a "word?" is considered "word" or "word-word" is considered "wordword".
* To speed up the search process, words are stored sorted without duplicates (TreeSet).
* The search is not case sensitive. For instance, a "Kotlin" is equal to "kotlin".

## Solution

* The original content of the file is not stored. There's no need to store it (at the moment) and it might just waste memory.
* The binary algorithm was used as a search engine. The downside is that the application will startup longer because the words will be sorted.
* BeanFactory + BeanProvider was used as a "Dependency Injector". It's no ideal solution.
* Search results are stored to speed up the search process of already searched items.

## Possible improvements

* Initializer could be multi-threaded (no need to read file one by one).
* Searches could be asynchronous - no need to wait for one search to start another one.
* Binary algorithm could be expanded to Multi-Key Binary algorithm to speed up process even more.

## Packages

* application - contains classes that orchestrates domain.
* domain - contains all domain logic (document, search result and search itself).
* infrastructure - contains classes that are needed for domain and application to work.
* ui - contains classes that display instructions and read commands from user.

## Statement

* Tests were run on MacOS :)