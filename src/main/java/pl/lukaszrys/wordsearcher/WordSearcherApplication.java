package pl.lukaszrys.wordsearcher;

import pl.lukaszrys.wordsearcher.infrastructure.bean.BeanProvider;
import pl.lukaszrys.wordsearcher.infrastructure.logger.SystemLogger;

public class WordSearcherApplication {

    private static final SystemLogger logger = new SystemLogger();

    public static void main(final String[] args) {
        if (args.length != 1) {
            logger.info("Invalid number of arguments");
            return;
        }

        BeanProvider.initializer.saveDocumentsIn(args[0]);
        BeanProvider.consoleUserInterface.listen();
    }
}
