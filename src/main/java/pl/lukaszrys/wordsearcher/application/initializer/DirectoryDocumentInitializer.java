package pl.lukaszrys.wordsearcher.application.initializer;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import pl.lukaszrys.wordsearcher.domain.Document;
import pl.lukaszrys.wordsearcher.domain.WordExtractor;
import pl.lukaszrys.wordsearcher.domain.repository.DocumentRepository;
import pl.lukaszrys.wordsearcher.infrastructure.logger.SystemLogger;

public class DirectoryDocumentInitializer implements DocumentInitializer {

    private static final SystemLogger logger = new SystemLogger();
    private static final String TEXT_FILE_EXTENSION = ".txt";

    private final DocumentRepository documentRepository;
    private final DocumentBuilder documentBuilder;
    private final WordExtractor wordExtractor;

    public DirectoryDocumentInitializer(final DocumentRepository documentRepository,
        final DocumentBuilder documentBuilder, final WordExtractor wordExtractor) {
        this.documentRepository = documentRepository;
        this.documentBuilder = documentBuilder;
        this.wordExtractor = wordExtractor;
    }

    @Override
    public int saveDocumentsIn(final String location) {
        final Path directoryPath = getDirectoryPath(location);
        logger.info("Reading files from " + directoryPath + "....");
        final int result = saveDocumentsInDirectory(directoryPath);
        logger.info(result + " files read in directory " + directoryPath);
        return result;
    }

    private int saveDocumentsInDirectory(final Path directoryPath) {
        try (final Stream<Path> paths = Files.walk(directoryPath)) {

            return iteratePaths(paths);
        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    private int iteratePaths(final Stream<Path> paths) {
        final AtomicInteger count = new AtomicInteger(0);
        paths.filter(onlyTextFilesPredicate()).map(this::createDocument).forEach(document -> {
            documentRepository.save(document);
            count.incrementAndGet();
        });
        return count.get();
    }

    private Document createDocument(final Path path) {
        try (final Stream<String> lines = Files.lines(path)) {
            final List<String> words = lines.flatMap(wordExtractor::findWordsFromLine).collect(Collectors.toUnmodifiableList());

            return documentBuilder.from(path, words);
        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    private Path getDirectoryPath(final String directoryName) {
        return Paths.get(directoryName);
    }

    private Predicate<Path> onlyTextFilesPredicate() {
        return path -> path.toString().endsWith(TEXT_FILE_EXTENSION);
    }
}
