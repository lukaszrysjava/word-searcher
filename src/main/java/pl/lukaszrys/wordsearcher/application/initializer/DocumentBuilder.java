package pl.lukaszrys.wordsearcher.application.initializer;

import java.nio.file.Path;
import java.util.List;
import java.util.UUID;
import pl.lukaszrys.wordsearcher.domain.Document;

public class DocumentBuilder {

    public Document from(final Path path, final List<String> content) {
        return new Document(UUID.randomUUID(), path.getFileName().toString(), content);
    }
}
