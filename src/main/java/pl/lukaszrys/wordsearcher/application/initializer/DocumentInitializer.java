package pl.lukaszrys.wordsearcher.application.initializer;

public interface DocumentInitializer {

    int saveDocumentsIn(final String location);
}
