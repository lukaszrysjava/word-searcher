package pl.lukaszrys.wordsearcher.application.search;

import java.util.stream.Stream;
import pl.lukaszrys.wordsearcher.domain.SearchDocumentService;
import pl.lukaszrys.wordsearcher.domain.SearchResult;

public class SearchDocumentFacade {

    private final SearchDocumentService searchService;

    public SearchDocumentFacade(final SearchDocumentService searchService) {
        this.searchService = searchService;
    }

    public Stream<SearchResult> search(final String lineWithWords) {
        searchService.searchDocument(lineWithWords);

        return searchService.findResults(lineWithWords);
    }
}
