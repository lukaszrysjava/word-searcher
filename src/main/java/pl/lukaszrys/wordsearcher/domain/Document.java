package pl.lukaszrys.wordsearcher.domain;

import java.util.*;

public class Document {

    private final UUID id;
    private final String name;
    private final Set<String> sortedWords;

    public Document(final UUID id, final String name, final Collection<String> words) {
        this.id = id;
        this.name = name;
        this.sortedWords = new TreeSet<>(words);
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<String> getSortedWords() {
        return Collections.unmodifiableSet(sortedWords);
    }
}
