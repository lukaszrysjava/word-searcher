package pl.lukaszrys.wordsearcher.domain;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import pl.lukaszrys.wordsearcher.domain.repository.DocumentRepository;
import pl.lukaszrys.wordsearcher.domain.repository.SearchResultRepository;

public class DomainSearchDocumentService implements SearchDocumentService {

    private final SearchResultRepository searchResultRepository;
    private final DocumentRepository documentRepository;
    private final SearchResultBuilder searchResultBuilder;
    private final WordExtractor wordExtractor;

    public DomainSearchDocumentService(final SearchResultRepository searchResultRepository,
        final DocumentRepository documentRepository, final SearchResultBuilder searchResultBuilder,
        final WordExtractor wordExtractor) {
        this.searchResultRepository = searchResultRepository;
        this.documentRepository = documentRepository;
        this.searchResultBuilder = searchResultBuilder;
        this.wordExtractor = wordExtractor;
    }

    @Override
    public void searchDocument(final String lineWithWordsToFind) {

        final Set<String> sortedWordsToFind = getSortedWordsToFind(lineWithWordsToFind);
        if (validateIfSearchedForAlready(sortedWordsToFind)) {
            return;
        }
        final List<Document> allDocuments = documentRepository.getAllDocuments();

        allDocuments.stream()
            .map(document -> searchDocument(sortedWordsToFind, document))
            .forEach(searchResultRepository::save);
    }

    @Override
    public Stream<SearchResult> findResults(final String lineWithWordsToFind) {
        return findResults(getSortedWordsToFind(lineWithWordsToFind));
    }

    private Stream<SearchResult> findResults(final Set<String> wordsToFind) {
        return searchResultRepository.findResultsByWordsToFind(wordsToFind);
    }

    private boolean validateIfSearchedForAlready(final Set<String> wordsToFind) {
        return findResults(wordsToFind).count() > 0;
    }

    private Set<String> getSortedWordsToFind(final String lineWithWordsToFind) {
        return wordExtractor.findWordsFromLine(lineWithWordsToFind).collect(Collectors.toCollection(TreeSet::new));
    }

    private SearchResult searchDocument(final Set<String> sortedWordsToFind, final Document document) {
        final Set<String> sortedWords = document.getSortedWords();
        final Set<String> foundWords = sortedWordsToFind.stream()
            .filter(word -> Arrays.binarySearch(sortedWords.toArray(), word) >= 0)
            .collect(Collectors.toUnmodifiableSet());
        return searchResultBuilder.buildFrom(document, sortedWordsToFind, foundWords);
    }
}
