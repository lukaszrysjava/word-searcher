package pl.lukaszrys.wordsearcher.domain;

import java.util.stream.Stream;

public interface SearchDocumentService {

    void searchDocument(final String lineWithWordsToFind);

    Stream<SearchResult> findResults(final String lineWithWordsToFind);
}
