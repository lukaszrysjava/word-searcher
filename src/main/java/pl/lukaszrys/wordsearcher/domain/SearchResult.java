package pl.lukaszrys.wordsearcher.domain;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;

public class SearchResult implements Comparable<SearchResult> {

    private final UUID id;
    private final UUID documentId;
    private final String name;
    private final BigDecimal accuracy;
    private final Set<String> wordsToFind;
    private final Set<String> wordsFound;

    public SearchResult(final UUID id, final UUID documentId, final String name, final Collection<String> wordsToFind,
        final Collection<String> wordsFound) {
        this.id = id;
        this.documentId = documentId;
        this.name = name;
        this.wordsToFind = new TreeSet<>(wordsToFind);
        this.wordsFound = new TreeSet<>(wordsFound);
        this.accuracy = determineAccuracy(wordsToFind, wordsFound);
    }

    public UUID getDocumentId() {
        return documentId;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getAccuracy() {
        return accuracy;
    }

    public Set<String> getWordsToFind() {
        return Collections.unmodifiableSet(wordsToFind);
    }

    public Set<String> getWordsFound() {
        return Collections.unmodifiableSet(wordsFound);
    }

    @Override
    public int compareTo(final SearchResult other) {
        final int compare = other.accuracy.compareTo(accuracy);
        return compare != 0 ? compare : 1;
    }


    private BigDecimal determineAccuracy(final Collection<String> wordsToFind, final Collection<String> wordsFound) {
        return BigDecimal.valueOf(wordsFound.size())
            .divide(BigDecimal.valueOf(wordsToFind.size()), MathContext.DECIMAL32);
    }
}
