package pl.lukaszrys.wordsearcher.domain;

import java.util.Collection;
import java.util.UUID;

public class SearchResultBuilder {

    public SearchResult buildFrom(final Document document, final Collection<String> wordsToFind,
        final Collection<String> foundWords) {
        return new SearchResult(UUID.randomUUID(), document.getId(), document.getName(), wordsToFind, foundWords);
    }

}
