package pl.lukaszrys.wordsearcher.domain;

import java.util.Arrays;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class WordExtractor {

    public Stream<String> findWordsFromLine(final String line) {
        return Arrays.stream(line.split(" "))
            .filter(this::notContainDigit)
            .map(this::removeNonLetter)
            .filter(notBlank());
    }

    private Predicate<String> notBlank() {
        return word -> !word.equals("");
    }

    private String removeNonLetter(final String word) {
        return word.replaceAll("[^a-zA-Z]", "").toLowerCase();
    }

    private boolean notContainDigit(final String word) {
        return !word.matches(".*\\d.*");
    }
}
