package pl.lukaszrys.wordsearcher.domain.repository;

import java.util.List;
import pl.lukaszrys.wordsearcher.domain.Document;

public interface DocumentRepository {

    List<Document> getAllDocuments();

    void save(Document document);
}
