package pl.lukaszrys.wordsearcher.domain.repository;

import java.util.Set;
import java.util.stream.Stream;
import pl.lukaszrys.wordsearcher.domain.SearchResult;

public interface SearchResultRepository {

    void save(SearchResult searchResult);

    Stream<SearchResult> findResultsByWordsToFind(final Set<String> wordsToFind);
}
