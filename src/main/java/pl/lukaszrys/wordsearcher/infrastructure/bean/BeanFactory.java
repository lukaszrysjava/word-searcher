package pl.lukaszrys.wordsearcher.infrastructure.bean;

import java.util.List;
import pl.lukaszrys.wordsearcher.application.initializer.DirectoryDocumentInitializer;
import pl.lukaszrys.wordsearcher.application.initializer.DocumentBuilder;
import pl.lukaszrys.wordsearcher.application.initializer.DocumentInitializer;
import pl.lukaszrys.wordsearcher.application.search.SearchDocumentFacade;
import pl.lukaszrys.wordsearcher.domain.DomainSearchDocumentService;
import pl.lukaszrys.wordsearcher.domain.SearchResultBuilder;
import pl.lukaszrys.wordsearcher.domain.WordExtractor;
import pl.lukaszrys.wordsearcher.domain.repository.DocumentRepository;
import pl.lukaszrys.wordsearcher.domain.repository.SearchResultRepository;
import pl.lukaszrys.wordsearcher.infrastructure.logger.SystemLogger;
import pl.lukaszrys.wordsearcher.infrastructure.repository.InMemoryDocumentRepository;
import pl.lukaszrys.wordsearcher.infrastructure.repository.InMemorySearchResultRepository;
import pl.lukaszrys.wordsearcher.ui.ConsoleUserInterface;

class BeanFactory {

    static DocumentBuilder documentBuilder() {
        return new DocumentBuilder();
    }

    static DocumentInitializer documentDirectoryInitializer(final DocumentBuilder documentBuilder,
        final DocumentRepository documentRepository, final WordExtractor wordExtractor) {
        return new DirectoryDocumentInitializer(documentRepository, documentBuilder, wordExtractor);
    }

    static DocumentRepository documentRepository() {
        return new InMemoryDocumentRepository(List.of());
    }

    static SearchResultRepository searchResultRepository() {
        return new InMemorySearchResultRepository(List.of());
    }

    static DomainSearchDocumentService domainSearchDocumentService(final SearchResultRepository searchResultRepository,
        final DocumentRepository documentRepository, final SearchResultBuilder searchResultBuilder,
        final WordExtractor wordExtractor) {
        return new DomainSearchDocumentService(searchResultRepository, documentRepository, searchResultBuilder,
            wordExtractor);
    }

    static SearchDocumentFacade searchDocumentFacade(final DomainSearchDocumentService domainSearchDocumentService) {
        return new SearchDocumentFacade(domainSearchDocumentService);
    }

    static SearchResultBuilder searchResultBuilder() {
        return new SearchResultBuilder();
    }

    static WordExtractor wordUtil() {
        return new WordExtractor();
    }

    static ConsoleUserInterface consoleUI(final SearchDocumentFacade searchDocumentFacade) {
        return new ConsoleUserInterface(searchDocumentFacade, new SystemLogger());
    }
}
