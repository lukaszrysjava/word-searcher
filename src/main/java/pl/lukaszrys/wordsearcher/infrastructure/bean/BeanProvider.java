package pl.lukaszrys.wordsearcher.infrastructure.bean;

import pl.lukaszrys.wordsearcher.application.initializer.DocumentBuilder;
import pl.lukaszrys.wordsearcher.application.initializer.DocumentInitializer;
import pl.lukaszrys.wordsearcher.application.search.SearchDocumentFacade;
import pl.lukaszrys.wordsearcher.domain.DomainSearchDocumentService;
import pl.lukaszrys.wordsearcher.domain.SearchResultBuilder;
import pl.lukaszrys.wordsearcher.domain.WordExtractor;
import pl.lukaszrys.wordsearcher.domain.repository.DocumentRepository;
import pl.lukaszrys.wordsearcher.domain.repository.SearchResultRepository;
import pl.lukaszrys.wordsearcher.ui.ConsoleUserInterface;

public class BeanProvider {

    private static final DocumentBuilder documentBuilder = BeanFactory.documentBuilder();
    private static final DocumentRepository documentRepository = BeanFactory.documentRepository();
    private static final WordExtractor wordExtractor = BeanFactory.wordUtil();
    public static final DocumentInitializer initializer = BeanFactory.documentDirectoryInitializer(documentBuilder,
        documentRepository, wordExtractor);
    private static final SearchResultBuilder searchResultBuilder = BeanFactory.searchResultBuilder();
    private static final SearchResultRepository searchResultRepository = BeanFactory.searchResultRepository();
    private static final DomainSearchDocumentService domainSearchDocumentService =
        BeanFactory.domainSearchDocumentService(
        searchResultRepository, documentRepository, searchResultBuilder, wordExtractor);
    public static final SearchDocumentFacade searchDocumentFacade = BeanFactory.searchDocumentFacade(
        domainSearchDocumentService);
    public static final ConsoleUserInterface consoleUserInterface = BeanFactory.consoleUI(searchDocumentFacade);
}
