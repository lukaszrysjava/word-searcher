package pl.lukaszrys.wordsearcher.infrastructure.logger;

import java.math.BigDecimal;
import pl.lukaszrys.wordsearcher.domain.SearchResult;

public class SystemLogger {

    public void info(final String message) {
        System.out.println(message);
    }

    public void command(final String message) {
        System.out.print(message);
    }

    public void result(final SearchResult result) {
        info(result.getName() + " " + result.getAccuracy().multiply(BigDecimal.valueOf(100)) + "%");
    }
}
