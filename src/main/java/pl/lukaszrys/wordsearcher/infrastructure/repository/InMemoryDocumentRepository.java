package pl.lukaszrys.wordsearcher.infrastructure.repository;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import pl.lukaszrys.wordsearcher.domain.Document;
import pl.lukaszrys.wordsearcher.domain.repository.DocumentRepository;

public class InMemoryDocumentRepository implements DocumentRepository {

    private final List<Document> memory;

    public InMemoryDocumentRepository(final List<Document> memory) {
        this.memory = new CopyOnWriteArrayList<>(memory);
    }

    @Override
    public List<Document> getAllDocuments() {
        return Collections.unmodifiableList(memory);
    }

    @Override
    public void save(final Document document) {
        memory.add(document);
    }
}
