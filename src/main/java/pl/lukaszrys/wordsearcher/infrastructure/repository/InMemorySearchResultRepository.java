package pl.lukaszrys.wordsearcher.infrastructure.repository;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;
import pl.lukaszrys.wordsearcher.domain.SearchResult;
import pl.lukaszrys.wordsearcher.domain.repository.SearchResultRepository;

public class InMemorySearchResultRepository implements SearchResultRepository {

    private final Set<SearchResult> memory;

    public InMemorySearchResultRepository(final Collection<SearchResult> memory) {
        this.memory = new TreeSet<>(memory);
    }

    @Override
    public void save(final SearchResult searchResult) {
        memory.add(searchResult);
    }

    @Override
    public Stream<SearchResult> findResultsByWordsToFind(final Set<String> wordsToFind) {
        return memory.stream().filter(searchResult -> searchResult.getWordsToFind().equals(wordsToFind));
    }
}
