package pl.lukaszrys.wordsearcher.ui;

import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import pl.lukaszrys.wordsearcher.application.search.SearchDocumentFacade;
import pl.lukaszrys.wordsearcher.domain.SearchResult;
import pl.lukaszrys.wordsearcher.infrastructure.logger.SystemLogger;

public class ConsoleUserInterface {

    private static final String SEARCH_INFO = "search>";
    private static final String QUIT_COMMAND = ":quit";

    private final SearchDocumentFacade searchDocumentFacade;
    private final SystemLogger logger;

    public ConsoleUserInterface(final SearchDocumentFacade searchDocumentFacade, final SystemLogger logger) {
        this.searchDocumentFacade = searchDocumentFacade;
        this.logger = logger;
    }

    public void listen() {
        final Scanner scan = new Scanner(System.in);
        do {
            logger.command(SEARCH_INFO);
        } while (!handleUserCommand(scan));
    }

    boolean handleUserCommand(final Scanner scan) {
        final String value = readValueFromScanner(scan);
        if (value.equals(QUIT_COMMAND)) {
            return true;
        }
        process(value);
        return false;
    }

    private String readValueFromScanner(final Scanner scan) {
        return scan.nextLine();
    }

    private void process(final String value) {
        final List<SearchResult> results = searchDocumentFacade.search(value)
            .limit(10)
            .filter(searchResult -> searchResult.getAccuracy().compareTo(BigDecimal.ZERO) > 0)
            .collect(Collectors.toUnmodifiableList());

        if (results.size() == 0) {
            logger.info("no matches found");
        } else {
            results.forEach(logger::result);
        }
    }
}
