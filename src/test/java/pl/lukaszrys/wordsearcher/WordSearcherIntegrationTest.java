package pl.lukaszrys.wordsearcher;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import pl.lukaszrys.wordsearcher.domain.SearchResult;
import pl.lukaszrys.wordsearcher.infrastructure.bean.BeanProvider;

@Tag("IT")
class WordSearcherIntegrationTest {

    @Test
    @DisplayName("Should initialize data then find words")
    void shouldInitializeDataThenFindWords() {
        BeanProvider.initializer.saveDocumentsIn(getDirectoryPath().toString());
        final String existingWord = "Kotlin";
        final String nonExistingWord = "dontExist";
        final String line = existingWord + " " + nonExistingWord;

        final List<SearchResult> result = BeanProvider.searchDocumentFacade.search(line)
            .collect(Collectors.toUnmodifiableList());

        filterFile(result, "test1.txt").forEach(searchResult -> {
            assertEquals(BigDecimal.valueOf(0.5), searchResult.getAccuracy());
            assertEquals(Set.of(existingWord.toLowerCase()), searchResult.getWordsFound());
            assertEquals(Set.of(existingWord.toLowerCase(), nonExistingWord.toLowerCase()),
                searchResult.getWordsToFind());
        });
        filterFile(result, "test3.txt").forEach(searchResult -> {
            assertEquals(BigDecimal.valueOf(0), searchResult.getAccuracy());
            assertEquals(Set.of(), searchResult.getWordsFound());
            assertEquals(Set.of(existingWord.toLowerCase(), nonExistingWord.toLowerCase()),
                searchResult.getWordsToFind());
        });
        assertTrue(filterFile(result, "test3.notTxt").findFirst().isEmpty());
    }

    private Stream<SearchResult> filterFile(final List<SearchResult> result, final String fileName) {
        return result.stream().filter(searchResult -> searchResult.getName().equals(fileName));
    }

    private Path getDirectoryPath() {
        return Paths.get("src", "test", "resources", "documentInitializerDirectory");
    }
}