package pl.lukaszrys.wordsearcher.application.initializer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.lukaszrys.wordsearcher.domain.Document;
import pl.lukaszrys.wordsearcher.domain.WordExtractor;
import pl.lukaszrys.wordsearcher.domain.repository.DocumentRepository;

class DirectoryDocumentInitializerTest {

    private DocumentRepository documentRepository;
    private DocumentBuilder documentBuilder;
    private WordExtractor wordExtractor;
    private DirectoryDocumentInitializer tested;

    @BeforeEach
    void setUp() {
        documentRepository = mock(DocumentRepository.class);
        documentBuilder = mock(DocumentBuilder.class);
        wordExtractor = mock(WordExtractor.class);

        tested = new DirectoryDocumentInitializer(documentRepository, documentBuilder, wordExtractor);
    }

    @Test
    @DisplayName("Should load test1.txt and test3.txt from directory")
    void shouldLoadTextFiles() {
        final Path directoryPath = getDirectoryPath();
        final Document test1Document = mock(Document.class);
        final Document test3Document = mock(Document.class);

        when(documentBuilder.from(getFileInDirectoryPath("/test1.txt"), getTest1Content())).thenReturn(test1Document);
        when(documentBuilder.from(getFileInDirectoryPath("/test3.txt"), getTest2Content())).thenReturn(test3Document);
        when(wordExtractor.findWordsFromLine("I am test1 and I contain \"Kotlin\" word? Additionally, te9st and 9!")).thenReturn(
            getTest1Content().stream());
        when(wordExtractor.findWordsFromLine("I am test3 and I contain \"Java\".")).thenReturn(getTest2Content().stream());

        final int result = tested.saveDocumentsIn(directoryPath.toString());

        assertEquals(2, result);
        verify(documentRepository).save(test1Document);
        verify(documentRepository).save(test3Document);
    }

    private List<String> getTest1Content() {
        return List.of("i", "am", "and", "i", "contain", "kotlin", "word", "additionally", "and");
    }

    private List<String> getTest2Content() {
        return List.of("i", "am", "and", "i", "contain", "java");
    }

    private Path getDirectoryPath() {
        return Paths.get("src", "test", "resources", "documentInitializerDirectory");
    }

    private Path getFileInDirectoryPath(final String fileName) {
        return Paths.get(getDirectoryPath() + fileName);
    }
}