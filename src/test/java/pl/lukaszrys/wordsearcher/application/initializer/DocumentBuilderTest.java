package pl.lukaszrys.wordsearcher.application.initializer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.lukaszrys.wordsearcher.domain.Document;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DocumentBuilderTest {

    private final DocumentBuilder tested = new DocumentBuilder();

    @Test
    @DisplayName("Should build document from path and words")
    public void shouldBuildDocumentFromPathAndWords() {
        final String fileName = "fileName.txt";
        final List<String> words = List.of("z", "a");
        final Path path = Paths.get("src", "test", fileName);

        final Document result = tested.from(path, words);

        assertEquals(fileName, result.getName());
        assertEquals(new TreeSet<>(words), result.getSortedWords());
    }
}