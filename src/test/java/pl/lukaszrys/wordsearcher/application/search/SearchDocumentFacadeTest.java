package pl.lukaszrys.wordsearcher.application.search;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.lukaszrys.wordsearcher.domain.SearchDocumentService;
import pl.lukaszrys.wordsearcher.domain.SearchResult;

class SearchDocumentFacadeTest {

    private SearchDocumentService searchDocumentService;
    private SearchDocumentFacade tested;

    @BeforeEach
    void setUp() {
        searchDocumentService = mock(SearchDocumentService.class);
        tested = new SearchDocumentFacade(searchDocumentService);
    }

    @Test
    @DisplayName("Should init search and then find result")
    void search() {
        final String lineWithWords = "I have words.";
        final SearchResult searchResult = mock(SearchResult.class);

        when(searchDocumentService.findResults(lineWithWords)).thenReturn(Stream.of(searchResult));
        final Stream<SearchResult> search = tested.search(lineWithWords);

        verify(searchDocumentService).searchDocument(lineWithWords);
        assertTrue(search.allMatch(result -> result.equals(searchResult)));
    }
}