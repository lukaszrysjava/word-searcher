package pl.lukaszrys.wordsearcher.domain;

import static org.mockito.Mockito.*;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.lukaszrys.wordsearcher.domain.repository.DocumentRepository;
import pl.lukaszrys.wordsearcher.domain.repository.SearchResultRepository;

class DomainSearchDocumentServiceTest {

    private SearchResultRepository searchResultRepository;
    private DocumentRepository documentRepository;
    private SearchResultBuilder searchResultBuilder;
    private DomainSearchDocumentService tested;
    private WordExtractor wordExtractor;

    @BeforeEach
    void setUp() {
        searchResultRepository = mock(SearchResultRepository.class);
        documentRepository = mock(DocumentRepository.class);
        searchResultBuilder = mock(SearchResultBuilder.class);
        wordExtractor = mock(WordExtractor.class);

        tested = new DomainSearchDocumentService(searchResultRepository, documentRepository, searchResultBuilder,
            wordExtractor);
    }

    @Test
    @DisplayName("Should search document and store search result")
    void searchDocument() {
        final String firstWord = "first";
        final String secondWord = "second";
        final String lineWithWords = "first second";
        final Set<String> wordsToFind = Set.of("first", "second");
        final Document firstDocument = buildDocument("#1", List.of(firstWord, "testC", secondWord));
        final Document secondDocument = buildDocument("#2", List.of("testA", "testC", secondWord));
        final Document thirdDocument = buildDocument("#3", List.of("testA", "testB", "testL"));
        final SearchResult first = mock(SearchResult.class);
        final SearchResult second = mock(SearchResult.class);
        final SearchResult third = mock(SearchResult.class);

        when(documentRepository.getAllDocuments()).thenReturn(List.of(firstDocument, secondDocument, thirdDocument));
        when(searchResultBuilder.buildFrom(firstDocument, wordsToFind, Set.of(firstWord, secondWord))).thenReturn(
            first);
        when(searchResultBuilder.buildFrom(secondDocument, wordsToFind, Set.of(secondWord))).thenReturn(second);
        when(searchResultBuilder.buildFrom(thirdDocument, wordsToFind, Set.of())).thenReturn(third);
        when(wordExtractor.findWordsFromLine(lineWithWords)).thenReturn(wordsToFind.stream());

        tested.searchDocument(lineWithWords);

        verify(searchResultRepository).save(first);
        verify(searchResultRepository).save(third);
        verify(searchResultRepository).save(second);

    }

    private Document buildDocument(final String name, final Collection<String> content) {
        return new Document(UUID.randomUUID(), name, content);
    }
}