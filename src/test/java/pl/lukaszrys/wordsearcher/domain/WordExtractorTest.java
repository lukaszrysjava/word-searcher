package pl.lukaszrys.wordsearcher.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class WordExtractorTest {

    private final WordExtractor tested = new WordExtractor();

    private static Stream<Arguments> provideLinesWithWords() {
        return Stream.of(Arguments.of("I! \"contain\" only, words.", List.of("i", "contain", "only", "words")),
            Arguments.of("test9 9 a3 3a # !", List.of()), Arguments.of("word and, n0t w0rd", List.of("word", "and")));
    }

    @ParameterizedTest
    @DisplayName("Should extract words from line")
    @MethodSource("provideLinesWithWords")
    void fromLine(final String line, final List<String> expected) {
        final List<String> result = tested.findWordsFromLine(line).collect(Collectors.toUnmodifiableList());

        assertEquals(expected, result);
    }
}