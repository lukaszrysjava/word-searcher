package pl.lukaszrys.wordsearcher.infrastructure.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.lukaszrys.wordsearcher.domain.Document;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class InMemoryDocumentRepositoryTest {

    private Document document;
    private InMemoryDocumentRepository tested;

    @BeforeEach
    void setUp() {
        document = new Document(UUID.randomUUID(), "test", List.of());
        tested = new InMemoryDocumentRepository(List.of(document));
    }

    @Test
    @DisplayName("Should get all documents")
    void getAllDocuments() {
        final List<Document> allDocuments = tested.getAllDocuments();

        assertEquals(1, allDocuments.size());
        assertTrue(allDocuments.contains(document));
    }

    @Test
    @DisplayName("Should save document")
    void save() {
        final Document newDocument = new Document(UUID.randomUUID(), "newDocument", List.of());

        tested.save(newDocument);
        final List<Document> allDocuments = tested.getAllDocuments();

        assertEquals(2, allDocuments.size());
        assertTrue(allDocuments.contains(document));
        assertTrue(allDocuments.contains(newDocument));
    }
}