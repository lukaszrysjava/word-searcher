package pl.lukaszrys.wordsearcher.infrastructure.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.lukaszrys.wordsearcher.domain.SearchResult;

class InMemorySearchResultRepositoryTest {

    private SearchResult inserted1 = new SearchResult(UUID.randomUUID(), UUID.randomUUID(), "name", List.of("test"), List.of());
    private SearchResult inserted2 = new SearchResult(UUID.randomUUID(), UUID.randomUUID(), "name", List.of("diff"), List.of());

    private InMemorySearchResultRepository tested;

    @BeforeEach
    void setUp() {
        tested = new InMemorySearchResultRepository(List.of(inserted1, inserted2));
    }

    @Test
    @DisplayName("Should find all with test word")
    void shouldFindAll() {
        final List<SearchResult> results = tested.findResultsByWordsToFind(inserted1.getWordsToFind())
            .collect(Collectors.toUnmodifiableList());

        assertEquals(1, results.size());
        assertTrue(results.contains(inserted1));
    }

    @Test
    @DisplayName("Should save search result and find it")
    void shouldSaveSearchResult() {
        final SearchResult toAdd = new SearchResult(UUID.randomUUID(), UUID.randomUUID(), "name2", inserted1.getWordsToFind(), List.of());

        tested.save(toAdd);
        final List<SearchResult> results = tested.findResultsByWordsToFind(inserted1.getWordsToFind())
            .collect(Collectors.toUnmodifiableList());

        assertEquals(2, results.size());
        assertTrue(results.contains(inserted1));
        assertTrue(results.contains(toAdd));
    }
}