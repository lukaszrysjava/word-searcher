package pl.lukaszrys.wordsearcher.ui;

import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.lukaszrys.wordsearcher.application.search.SearchDocumentFacade;
import pl.lukaszrys.wordsearcher.domain.SearchResult;
import pl.lukaszrys.wordsearcher.infrastructure.logger.SystemLogger;

class ConsoleUserInterfaceTest {

    private SearchDocumentFacade searchDocumentFacade;
    private SystemLogger systemLogger;
    private ConsoleUserInterface tested;

    @BeforeEach
    void setUp() {
        searchDocumentFacade = mock(SearchDocumentFacade.class);
        systemLogger = mock(SystemLogger.class);
        tested = new ConsoleUserInterface(searchDocumentFacade, systemLogger);
    }

    @Test
    @DisplayName("Should accept scanner input and get results")
    void listen() {
        final String input = "test";
        final SearchResult valid = buildSearchResult(input, "name#1", List.of(input));
        final SearchResult invalid = buildSearchResult(input, "name#2", List.of());

        when(searchDocumentFacade.search(input)).thenReturn(Stream.of(valid, invalid));

        tested.handleUserCommand(new Scanner(input));

        verify(systemLogger).result(valid);
        verify(systemLogger, times(0)).result(invalid);

    }

    private SearchResult buildSearchResult(final String input, final String name, final List<String> wordsFound) {
        return new SearchResult(UUID.randomUUID(), UUID.randomUUID(), name, List.of(input), wordsFound);
    }
}